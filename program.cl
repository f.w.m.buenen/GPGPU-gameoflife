#define GLINTEROP;

// screen dimensions
__constant int WIDTH = 578;
__constant int HEIGHT = 545;

uint GetBit(uint x, uint y, int pw, __global uint* buffer)  { return (buffer[y * pw + (x >> 5)] >> (int)(x & 31)) & 1U; }
void BitSet(uint x, uint y, int pw, __global uint* buffer) { atomic_or(&buffer[y * pw + (x >> 5)], (1U << (int)(x & 31))); }

#ifdef GLINTEROP
__kernel void simulate( __global uint* pattern, __global uint* second,
                        int pw, int ph, write_only image2d_t a,
						uint xoffset, uint yoffset, float zoom)
#else
__kernel void simulate( __global uint* pattern, __global uint* second,
                               int pw, int ph)
#endif
{	
	// get thread id
	int id = get_global_id( 0 );

	// check if it fits pattern boundary
	if (id >= pw*ph*32) return;

	// calculate x and y
	uint x = id % (pw*32);
	uint y = (id - x) / (pw*32);

	// one pixel boundary
	if(x == 0 || x == pw*32 - 1 || y == 0 || y == ph-1) return;

	// do simulation
	uint n = GetBit( x - 1, y - 1, pw, second ) + GetBit( x, y - 1, pw, second ) + GetBit( x + 1, y - 1, pw, second ) + 
	GetBit( x - 1, y, pw, second ) + GetBit( x + 1, y, pw, second ) + GetBit( x - 1, y + 1, pw, second ) + GetBit( x, y + 1, pw, second ) + 
	GetBit( x + 1, y + 1, pw, second );
	if ((GetBit( x, y, pw, second ) == 1 && n == 2) || n == 3) BitSet( x, y, pw, pattern );

#ifdef GLINTEROP
	// check if x and y fit within screen dimensions
	if (x > WIDTH || y > HEIGHT) return;
	int bitX = x/zoom + xoffset;
	int bitY = y/zoom + yoffset;

	// check if bit positions fit pattern dimensions
	bool inPattern = !(bitX > pw*32 || bitY > ph);
	int2 pos = (int2)(x, y);

	if (GetBit( bitX, bitY, pw, second ) == 1 && inPattern)
		write_imagef( a, pos, (float4)(1.0f, 1.0f, 1.0f, 1.0f ) );
	else
		write_imagef( a, pos, (float4)(0.0f, 0.0f, 0.0f, 1.0f ) );
#endif
}

// swaps buffers and clears pattern
__kernel void swap( __global uint* pattern, __global uint* second,
					int pw, int ph)
{
	// get thread id
	int id = get_global_id( 0 );

	// check buffer index
	if (id >= pw*ph) return;

	second[id] = pattern[id];
	pattern[id] = 0;
}