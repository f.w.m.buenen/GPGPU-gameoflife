﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Cloo;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Template {

    class Game
    {
	    // when GLInterop is set to true, the fractal is rendered directly to an OpenGL texture
	    bool GLInterop = true;
	    // load the OpenCL program; this creates the OpenCL context
	    static OpenCLProgram ocl = new OpenCLProgram( "../../program.cl" );
	    // find the kernel named 'simulate', 'swap' and 'clear' in the program
	    OpenCLKernel simKernel  = new OpenCLKernel(ocl, "simulate" );
        OpenCLKernel swapKernel = new OpenCLKernel(ocl, "swap");

        // create a regular buffer; by default this resides on both the host and the device
        OpenCLBuffer<uint> patternBuffer;
        OpenCLBuffer<uint> secondBuffer;

        // create an OpenGL texture to which OpenCL can send data
        OpenCLImage<int> image;
	    public Surface screen;
	    Stopwatch timer = new Stopwatch();

        int generation = 0;

        uint pw, ph; // note: pw is in uints; width in bits is 32 this value.
        // helper function for setting one bit in the pattern buffer
        void BitSet(uint x, uint y) { patternBuffer[(int)(y * pw + (x >> 5))] |= 1U << (int)(x & 31); }
        // helper function for getting one bit from the secondary pattern buffer
        uint GetBit(uint x, uint y) { return secondBuffer[(int)(y * pw + (x >> 5))] >> (int)(x & 31) & 1U; }
        // mouse handling: dragging functionality
        uint xoffset = 0, yoffset = 0;
        int zoomXOffset, zoomYOffset, zoomXOffsetOld, zoomYOffsetOld;

        // zoom variable; one cell from buffer applies to zoom*zoom grid
        float zoom = 1f;
        float zoomFactor = 0.05f;
        bool moveableX, moveableY; // if camera can be moved depends on zoom factor

        bool lastLButtonState = false;
        int dragXStart, dragYStart, offsetXStart, offsetYStart;

        // minimalistic .rle file reader for Golly files (see http://golly.sourceforge.net)
        public void Init()
        {
            // make image fit screen
            image = new OpenCLImage<int>(ocl, screen.width, screen.height);

            StreamReader sr = new StreamReader("../../data/turing_js_r.rle");
            uint state = 0, n = 0, x = 0, y = 0;
            while (true)
            {
                String line = sr.ReadLine();
                if (line == null) break; // end of file
                int pos = 0;
                if (line[pos] == '#') continue;
                else if (line[pos] == 'x') // header
                {
                    String[] sub = line.Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);
                    pw = (UInt32.Parse(sub[1]) + 31) / 32;
                    ph = UInt32.Parse(sub[3]);

                    // initialize OpenCL buffers
                    patternBuffer = new OpenCLBuffer<uint>(ocl, (int)(pw * ph));
                    secondBuffer = new OpenCLBuffer<uint>(ocl, (int)(pw * ph));
                }
                else while (pos < line.Length)
                    {
                        Char c = line[pos++];
                        if (state == 0) if (c < '0' || c > '9') { state = 1; n = Math.Max(n, 1); } else n = (uint)(n * 10 + (c - '0'));
                        if (state == 1) // expect other character
                        {
                            if (c == '$') { y += n; x = 0; } // newline
                            else if (c == 'o') for (int i = 0; i < n; i++) BitSet(x++, y); else if (c == 'b') x += n;
                            state = n = 0;
                        }
                    }
            }

            // initialize moveable vars
            UpdateZoom(0);

            // swap buffers
            for (int i = 0; i < pw * ph; i++)
            {
                secondBuffer[i] = patternBuffer[i];
                patternBuffer[i] = 0;
            }

            // copy buffers to GPU
            patternBuffer.CopyToDevice();
            secondBuffer.CopyToDevice();

            // set simulation arguments
            simKernel.SetArgument(0, patternBuffer);
            simKernel.SetArgument(1, secondBuffer);
            simKernel.SetArgument(2, pw);
            simKernel.SetArgument(3, ph);
            if (GLInterop)
            {
                simKernel.SetArgument(4, image);
                simKernel.SetArgument(5, xoffset);
                simKernel.SetArgument(6, yoffset);
                simKernel.SetArgument(7, zoom);
            }

            // set swap kernel arguments
            swapKernel.SetArgument(0, patternBuffer);
            swapKernel.SetArgument(1, secondBuffer);
            swapKernel.SetArgument(2, pw);
            swapKernel.SetArgument(3, ph);
        }

        public void Tick()
	    {
            // start timer
            timer.Restart();

            GL.Finish();

            // clear the screen and visualize current state
            screen.Clear(0);

            if(!GLInterop)
            {
                for (uint y = 0; y < screen.height; y++) for (uint x = 0; x < screen.width; x++)
                    {
                        int bitX = (int)(x / zoom) + (int)xoffset + zoomXOffset;
                        int bitY = (int)(y / zoom) + (int)yoffset + zoomYOffset;

                        if (GetBit(x + xoffset + (uint)zoomXOffset, y + yoffset + (uint)zoomYOffset) == 1)
                        {
                            screen.Plot((int)x, (int)y, 0xffffff);
                        }
                    }
            }

            // update zoom-specific offset
            zoomXOffsetOld = zoomXOffset;
            zoomYOffsetOld = zoomYOffset;
            zoomXOffset = Math.Max((int)(screen.width  / 2f - (screen.width  / (zoom * 2f))), 0);
            zoomYOffset = Math.Max((int)(screen.height / 2f - (screen.height / (zoom * 2f))), 0);

            long[] simWorkSize = { pw*ph*32 };
            long[] swapWorkSize = { pw * ph };
            long[] localSize = { 32, 4 };

		    if (GLInterop)
		    {
                // lock OpenGL texture
			    simKernel.LockOpenGLObject( image.texBuffer );

                // set kernel arguments
                simKernel.SetArgument(5, xoffset);
                simKernel.SetArgument(6, yoffset);
                simKernel.SetArgument(7, zoom);

                // execute the kernel
                simKernel.Execute( simWorkSize, localSize );
                swapKernel.Execute( swapWorkSize );
			    // unlock the OpenGL texture so it can be used for drawing a quad
			    simKernel.UnlockOpenGLObject( image.texBuffer );
		    }
		    else
		    {
                // execute the kernels
			    simKernel.Execute( simWorkSize );
                swapKernel.Execute( swapWorkSize );
			    
                // get the data from the device to the host
			    secondBuffer.CopyFromDevice();
                patternBuffer.CopyFromDevice();
            }


            // report performance
            Console.WriteLine("generation " + generation++ + ": " + timer.ElapsedMilliseconds + "ms");
        }

        public void Render() 
	    {
		    // use OpenGL to draw a quad using the texture that was filled by OpenCL
		    if (GLInterop)
		    {
			    GL.LoadIdentity();
			    GL.BindTexture( TextureTarget.Texture2D, image.OpenGLTextureID );
			    GL.Begin( PrimitiveType.Quads );
			    GL.TexCoord2( 0.0f, 1.0f ); GL.Vertex2( -1.0f, -1.0f );
			    GL.TexCoord2( 1.0f, 1.0f ); GL.Vertex2(  1.0f, -1.0f );
			    GL.TexCoord2( 1.0f, 0.0f ); GL.Vertex2(  1.0f,  1.0f );
			    GL.TexCoord2( 0.0f, 0.0f ); GL.Vertex2( -1.0f,  1.0f );
			    GL.End();
		    }
	    }

        public void SetMouseState(int x, int y, bool pressed)
        {
            if (pressed)
            {
                if (lastLButtonState)
                {
                    int deltax = (int)((x - dragXStart)/zoom), deltay = (int)((y - dragYStart)/zoom);
                    int deltaZoomX = zoomXOffset - zoomXOffsetOld;
                    int deltaZoomY = zoomYOffset - zoomYOffsetOld;

                    xoffset = moveableX ? (uint)Math.Min((pw * 32 - screen.width / zoom), Math.Max(0, offsetXStart - deltax + deltaZoomX)) : 0;
                    yoffset = moveableY ? (uint)Math.Min((ph - screen.height / zoom), Math.Max(0, offsetYStart - deltay + deltaZoomY)) : 0;
                }
                else
                {
                    dragXStart = x;
                    dragYStart = y;
                    offsetXStart = (int)xoffset;
                    offsetYStart = (int)yoffset;
                    lastLButtonState = true;
                }
            }
            else lastLButtonState = false;
        }

        public void UpdateZoom(int direction)
        {
            zoom += direction * zoomFactor;
            zoomFactor = zoom * 0.025f; // 0.025f == speed of zoom, used literal for simplicity

            // check if camera can be moved after zoom update
            float maxScreenX = pw * 32 - screen.width/zoom;
            float maxScreenY = ph - screen.height/zoom;
            moveableX = !(maxScreenX < 0);
            moveableY = !(maxScreenY < 0);

            // keep offset within pattern boundaries if zoom occurs
            if (screen.width  + xoffset > pw * 32 * zoom &&  moveableX) xoffset -= (uint)Math.Max((screen.width/zoom + xoffset + zoomXOffset - pw * 32), 0);
            if (screen.height + yoffset > ph *      zoom &&  moveableY) yoffset -= (uint)Math.Max((screen.height/zoom + yoffset + zoomYOffset - ph), 0);
            if (screen.width  + xoffset > pw * 32 * zoom && !moveableY) xoffset  = 0;
            if (screen.height + yoffset > ph *      zoom && !moveableY) yoffset  = 0;
        }

    }

} // namespace Template